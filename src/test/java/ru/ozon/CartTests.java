package ru.ozon;
import jdk.jfr.Description;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.*;

public class CartTests extends Assert {
    private final FirefoxOptions foxOptions = new FirefoxOptions();
    private FirefoxDriver foxDriver;
    private WebDriverWait wait;
    private TestHelper helper;

    private int productsCount;
    private HashMap<String, Product> products = new HashMap<>();

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.gecko.driver", "C:/WebDrivers/geckodriver.exe");
        foxOptions.addPreference("dom.webnotifications.enabled", false); // об этих всплывающих окнах шла речь?
        foxDriver = new FirefoxDriver(foxOptions);
        wait = new WebDriverWait(foxDriver, 10);
        helper = new TestHelper(foxDriver, wait);
    }

    @Test
    @Description("Открыть в браузере сайт https://www.ozon.ru/. Если откроется всплывающее окно – закрыть его.")
    public void openOzon() {
        foxDriver.get("https://www.ozon.ru/");
    }

    @Test(priority = 1)
    @Description("В меню \"Все разделы\" выбрать категорию \"Музыка\".")
    public void selectMusic() {
        foxDriver.findElementByXPath("//span[@data-test-id='menu-show-dropdown']").click();
        foxDriver.findElementByXPath("//a[@data-test-category-id='13100']").click();
    }

    @Test(priority = 2)
    @Description("С открывшейся страницы перейти на страницу \"Виниловые пластинки\".")
    public void gotoVinyl() {
        foxDriver.findElementByXPath(
                "//a[@class='title' and contains(text(),'Виниловые пластинки (LP)')]"
        ).click();
    }

    @Test(priority = 3)
    @Description("Проверить, что открылся список товаров.")
    public void productListExists() {
        Assert.assertTrue(helper.productListExists());
    }

    @Test(priority = 4)
    @Description("Получить количество товаров на странице.")
    public void saveProductsCount1() {
        productsCount = helper.getProductCount();
    }

    @Test(priority = 5)
    @Description("Выбрать товар под случайно сгенерированным номером (Перейти на страницу товара).")
    public void selectFirstProduct() {
        helper.selectRandomProduct(productsCount);
    }

    @Test(priority = 6)
    @Description("Запомнить стоимость и название данного товара.")
    public void saveFirstProduct() {
        products.put("FirstProduct", helper.getProduct());
    }

    @Test(priority = 7)
    @Description("Добавить товар в корзину.")
    public void addFirstProductToCart() {
        helper.addProductToCart();
    }

    @Test(priority = 8)
    @Description("Проверить то, что в корзине появился добавленный в п.9 товар. (Проверка данных\n" +
            "определенного товара. Необходим переход в корзину для этого.)")
    public void firstProductExistsInCart() {
        helper.gotoCart();
        Product expected = products.get("FirstProduct");
        Product actual = helper.getProductFromCart(0);

        Assert.assertEquals(actual.name, expected.name);
        assert actual.discountPrice.compareTo(expected.discountPrice) == 0
                : String.format("Ожидаемая цена (%s) не соответствует полученной (%s).",
                expected.discountPrice.toString(), actual.discountPrice.toString());
    }

    @Test(priority = 9)
    @Description("Вернуться на страницу \"Виниловые пластинки\".")
    public void backtoVinyl() {
        foxDriver.navigate().back();
        foxDriver.navigate().back();

        Assert.assertTrue(helper.productListExists());
    }

    @Test(priority = 10)
    @Description("Получить количество товаров на странице.")
    public void saveProductsCount2() {
        productsCount = helper.getProductCount();
    }

    @Test(priority = 11)
    @Description("Выбрать товар под случайно сгенерированным номером (Перейти на страницу товара).")
    public void selectSecondProduct() {
        helper.selectRandomProduct(productsCount);
    }

    @Test(priority = 12)
    @Description("Запомнить стоимость и название данного товара.")
    public void saveSecondProduct() {
        products.put("SecondProduct", helper.getProduct());
    }

    @Test(priority = 13)
    @Description("Добавить товар в корзину.")
    public void addSecondProductToCart() {
        helper.addProductToCart();
    }


    @Test(priority = 14)
    @Description("Проверить то, что в корзине два товара. (Проверка количества товаров в корзине. Может\n" +
            "быть произведена без открытия корзины, а проверяя значение в header сайта, где указано\n" +
            "количество товаров в корзине)")
    void cartContainsTwoProducts() {
        var cartHeaderXPath = "//span[@data-test-id='counter' and @class='count']";
        wait.until(ExpectedConditions.textToBe(By.xpath(cartHeaderXPath), "2"));
    }

    @Test(priority = 15)
    @Description("Открыть корзину. Проверить то, что в корзине раннее выбранные товары и итоговая стоимость по двум\n" +
            "товарам рассчитана верно.")
    public void VerifyProductsAndTotal() {
        Product firstExpected = products.get("FirstProduct");
        Product secondExpected = products.get("SecondProduct");
        helper.gotoCart();
        Product firstActual = helper.getProductFromCart(1);
        Product secondActual = helper.getProductFromCart(0);
        String formattedTotal = foxDriver
                .findElementByXPath("//div[@data-test-id='total-price-block']")
                .getText()
                .replaceAll("[^\\d.,]", "");
        BigDecimal actualTotal = new BigDecimal(formattedTotal);
        BigDecimal expectedTotal = firstExpected.discountPrice
                .add(secondExpected.discountPrice);

        Assert.assertEquals(firstActual.name, firstExpected.name);
        Assert.assertEquals(secondActual.name, secondExpected.name);
        assert actualTotal.compareTo(expectedTotal) == 0
                : String.format("Ожидаемая сумма (%s) не соответствует полученной (%s).",
                expectedTotal.toString(), actualTotal.toString());
    }

    @Test(priority = 16)
    @Description("Удалить из корзины все товары.")
    public void clearCart() {
        foxDriver.findElementByXPath("//button[@data-test-id='cart-delete-selected-btn']").click();
        foxDriver.findElementByXPath("//button[@data-test-id='checkcart-confirm-modal-confirm-button']").click();
    }

    @Test(priority = 17)
    @Description("Проверить, что корзина пуста.")
    public void cartEmpty() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@data-test-id='empty-cart']")));
    }

    @AfterClass
    public void endWork () {
        foxDriver.close();
    }
}

class Product {
    final String name;
    // Оставил, вдруг пригодятся
    final BigDecimal price;
    final BigDecimal discount;
    final BigDecimal discountPrice;

    Product (String name, BigDecimal price, BigDecimal discount) {
        this.name = name;
        this.price = price;
        this.discount = discount;
        this.discountPrice = price.subtract(discount);
    }
}

class TestHelper {
    private final FirefoxDriver foxDriver;
    private final WebDriverWait wait;

    TestHelper(FirefoxDriver driver, WebDriverWait wait) {
        foxDriver = driver;
        this.wait = wait;
    }

    boolean productListExists() {
        return foxDriver.findElementsByXPath("//div[@class='tiles']").size() > 0;
    }

    int getProductCount() {
        return foxDriver.findElementsByXPath("//div[@class='tile']").size();
    }

    void selectRandomProduct(int productsCount) {
        int randomProduct = new Random().nextInt(productsCount + 1);
        var xpath = String.format("//div[@data-index='%s']", randomProduct);
        foxDriver.findElementByXPath(xpath).click();
    }

    /**
     * Функция получения продукта из страницы описания.
     * @return возвращает экземпляр типа {@link Product}
     */
    Product getProduct() {
        String name = foxDriver.findElementByXPath("//h1[@itemprop='name']").getText();
        String formattedPrice = foxDriver
                .findElementByXPath("//span[@data-test-id='saleblock-first-price']")
                .getText()
                .replaceAll("[^\\d.,]", "");
        List<WebElement> discountList = foxDriver
                .findElementsByXPath("//div[@class='price-discount']//div[@class='main']");
        var price = new BigDecimal(formattedPrice);
        var totalDiscount = new BigDecimal(0);

        for (var disc:discountList) {
            String formattedDiscount = disc.getText().replaceAll("[^\\d.,]", "");
            totalDiscount = totalDiscount.add(new BigDecimal(formattedDiscount));
        }

        return new Product(name, price, totalDiscount);
    }

    void addProductToCart() {
        foxDriver.findElementByXPath("//button[@data-test-id='saleblock-add-to-cart-button']").click();
    }

    void gotoCart() {
        foxDriver.findElementByXPath("//div[@data-test-id='header-cart']/a").click();
    }

    /**
     * Функция получения продукта из указанной позиции корзины.
     * @param position позиция продукта.
     * @return возращает экземпляр типа {@link Product}
     */
    Product getProductFromCart(int position) {
        String cartItemXPath = "//a[@data-test-id='cart-item-title']";
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(cartItemXPath)));
        String name = foxDriver
                .findElementsByXPath(cartItemXPath)
                .get(position)
                .getText();
        WebElement priceBox = foxDriver
                .findElementsByXPath("//div[@class='box m-box-price']")
                .get(position);
        String formattedPrice = priceBox
                .findElement(By.xpath(".//span[@data-test-id='original-price-of-item-in-cart']"))
                .getText()
                .replaceAll("[^\\d.,]", "");
        List<WebElement> discountList = priceBox
                .findElements(By.xpath(".//span[@data-test-id='discount-of-item-in-split']"));

        var price = new BigDecimal(formattedPrice);
        var totalDiscount = new BigDecimal(0);

        for (var disc:discountList) {
            String formattedDiscount = disc.getText().replaceAll("[^\\d.,]", "");
            totalDiscount = totalDiscount.add(new BigDecimal(formattedDiscount));
        }

        return new Product(name, price, totalDiscount);
    }
}